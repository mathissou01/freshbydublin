
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Freshby - Introduction</title>
  <link rel="stylesheet" href="{{ ('../css/introduction.css') }}">
  
</head>
<body>
  <div class="transition"></div>
  <section class="rw-wrapper">
    <div class="rw-words rw-words-1">
      <span>Hello,
        @if ($player->gender == 'Boy')
         <p class="male">{{ $player->first_name }}</p>
         @else (player->gender == 'Girl')
          <p class="female">{{ $player->first_name }}</p>
        @endif
        I am <b>Mirallia</b></span>
      <span>The world is in danger ! <br><b>Billytrash</b> and his minions <br>are trying to take it over !</br></span>
      <span>
        Find my artefacts hidden in the three<br> sacred islands
        to help me defeat them</span>
      <span>
      Good luck,<br>
      I will be there to help you through <br>your journey ! 
      </span>
    </div>
  </section>

<div class="fire-wrapper">
  <img class="fire" src="https://www.stivaliserna.com/assets/rocket/fire.svg" />
</div>

<div class="rain rain1"></div>
<div class="rain rain2">
  <div class="drop drop2"></div>
</div>
<div class="rain rain3"></div>
<div class="rain rain4"></div>
<div class="rain rain5">
  <div class="drop drop5"></div>
</div>
<div class="rain rain6"></div>
<div class="rain rain7"></div>
<div class="rain rain8">
  <div class="drop drop8"></div>
</div>
<div class="rain rain9"></div>
<div class="rain rain10"></div>
<div class="drop drop11"></div>
<div class="drop drop12"></div>
<div id="canvas"></div>

<script src='https://unpkg.com/three@0.123.0/build/three.min.js'></script>
<script src='https://unpkg.com/three@0.123.0/examples/js/loaders/GLTFLoader.js'></script><script  src="./script.js"></script>

</body>
</html>
<script>
    let scene,
  camera,
  fieldOfView,
  aspectRatio,
  nearPlane,
  farPlane,
  renderer,
  container,
  rocket,
  composer,
  HEIGHT,
  WIDTH;

const createScene = () => {
  HEIGHT = window.innerHeight;
  WIDTH = window.innerWidth;

  scene = new THREE.Scene();

  scene.fog = new THREE.Fog(0x5d0361, 10, 1500);

  aspectRatio = WIDTH / HEIGHT;
  fieldOfView = 60;
  nearPlane = 1;
  farPlane = 10000;
  camera = new THREE.PerspectiveCamera(
    fieldOfView,
    aspectRatio,
    nearPlane,
    farPlane
  );

  camera.position.x = 0;
  camera.position.z = 500;
  camera.position.y = -10;

  renderer = new THREE.WebGLRenderer({
    alpha: true,
    antialias: true
  });
  renderer.setSize(WIDTH, HEIGHT);

  renderer.shadowMap.enabled = true;

  container = document.getElementById("canvas");
  container.appendChild(renderer.domElement);

  window.addEventListener("resize", handleWindowResize, false);

  let loader = new THREE.GLTFLoader();
  loader.load( "https://www.stivaliserna.com/assets/rocket/rocket.gltf",
    (gltf) => {
      rocket = gltf.scene;
      rocket.position.y = 50;
      scene.add(rocket);
    }
  );
};

const handleWindowResize = () => {
  HEIGHT = window.innerHeight;
  WIDTH = window.innerWidth;
  renderer.setSize(WIDTH, HEIGHT);
  camera.aspect = WIDTH / HEIGHT;
  camera.updateProjectionMatrix();
};

const createLights = () => {
  const ambientLight = new THREE.HemisphereLight(0x404040, 0x404040, 1);

  const directionalLight = new THREE.DirectionalLight(0xdfebff, 1);
  directionalLight.position.set(-300, 0, 600);

  const pointLight = new THREE.PointLight(0xa11148, 2, 1000, 2);
  pointLight.position.set(200, -100, 50);

  scene.add(ambientLight, directionalLight, pointLight);
};

const targetRocketPosition = 40;
const animationDuration = 2000;

const loop = () => {
  const t = (Date.now() % animationDuration) / animationDuration;

  renderer.render(scene, camera);

  const delta = targetRocketPosition * Math.sin(Math.PI * 2 * t);
  if (rocket) {
    rocket.rotation.y += 0.1;
    rocket.position.y = delta;
  }

  requestAnimationFrame(loop);
};

const main = () => {
  createScene();
  createLights();

  renderer.render(scene, camera);
  loop();
};

main();

//%ake the smoke of the rocket appear 20 secondes after the user start the page
setTimeout(() => {

 let cam, scene, renderer,
    clock, smokeMaterial,
    h, w,
    smokeParticles = [];

const animate = () => {
        let delta = clock.getDelta();

        requestAnimationFrame(animate);

        [].forEach.call(smokeParticles, sp => {
            sp.rotation.z += delta * 0.2;
        });

        renderer.render(scene, cam);
    },
    resize = () => {
        renderer.setSize(window.innerWidth, window.innerHeight);

    },
    lights = () => {
        const light = new THREE.DirectionalLight(0xff5fff, 0.5);

        clock = new THREE.Clock();

        renderer = new THREE.WebGLRenderer({
        alpha: true,
        antialias: true
        });
        renderer.setClearColor( 0xffffff, 0);
        renderer.setSize(w, h);

        scene = new THREE.Scene();
        scene.background = new THREE.Color( 0xffffff, 0 );
        light.position.set(-1, 0, 1);
        scene.add(light);
    },
    camera = () => {
        cam = new THREE.PerspectiveCamera(
            75,
            w / h,
            1,
            10000
        );

        cam.position.z = 1000;

        scene.add(cam);
    },
    action = () => {
        const loader = new THREE.TextureLoader();

        loader.crossOrigin = '';

        loader.load(
            'https://s3-us-west-2.amazonaws.com/s.cdpn.io/82015/blue-smoke.png',
            function onLoad(texture) {
                const smokeGeo = new THREE.PlaneBufferGeometry(300, 300);

                smokeMaterial = new THREE.MeshLambertMaterial({
                    map: texture,
                    transparent: true
                });

                for (let p = 0, l = 350; p < l; p++) {
                    let particle = new THREE.Mesh(smokeGeo, smokeMaterial);

                    particle.position.set(
                        Math.random() * 800 - 250,
                        Math.random() * 800 - 250,
                        Math.random() * 1000 - 100
                    );

                    particle.rotation.z = Math.random() * 390;
                    scene.add(particle);
                    smokeParticles.push(particle);
                }

                animate();
            }
        );

    },
    init = () => {
        h = window.innerHeight;
        w = window.innerWidth;
        
        lights(); //💡
        camera(); // 🎥
        action(); // 🎬

        document.body.appendChild(renderer.domElement);
        
    };

init();
}, 1)

  function endanimation() {

setTimeout(() => {
  window.location.href = "{{ route('show_maps', ['id'=>$player->id]) }}";

}, 18000)
}
endanimation();
  
    setTimeout(() => {
document.querySelector(".transition").style.transform = "scale(100) translate(-50%, -50%)";
    },17500)
</script>
