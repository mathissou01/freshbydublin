
<x-app-layout>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Freshby - Create your player</title>
    <link rel="stylesheet" href="{{ ('css/form.css') }}">
</head>
<body> 
    <div class="shade">  
        <div class="blackboard">
                <img src="{{ asset('/storage/images/20.svg') }}" class='tracecrait'>
             <div class="form">
                <form action="{{ route('store_player') }}" method="POST" id="player_form">
                    @csrf
                 
                    <label for="player">First name of the player :</label>
                        <input type="text" name="first_name" id="first_name" placeholder="Your first name">
                <br>
                        <label for="player">Do you live in a city or a village :</label>         
                            <select name="place" id="place" class="mt-1 block w-full sm:text-sm form-input rounded-md">>
                                <option value="">City / Village</option>
                                    <option value="City">City</option>
                                        <option value="Village">Village</option>                                                           
                                    </select>
                                 <br>
                                  
                    <label for="player">Are you a boy or a girl : </label>         
                        <select name="gender" id="gender" class="mt-1 block w-full sm:text-sm form-input rounded-md">>
                            <option value="">Boy / Girl</option>
                                <option value="Boy">Boy</option>
                                    <option value="Girl">Girl</option>                                                           
                        </select>
                     <br>
                        	<p class="wipeout">
                <button type="submit"> Start</button>
                             </p>
                </form>
            </div>
        </div>
    </div>
    <div class="rockimage">
        <img src="{{ asset('/storage/images/rock.png') }}" class='rock'>
    </div>
</body>
</html>
</x-app-layout>
@include('partials.footer')