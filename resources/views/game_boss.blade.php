s<x-app-layout>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Freshby - Boss</title>
  <link rel="stylesheet" href="{{ ('../css/boss.css') }}">
<script src='https://unpkg.com/three@0.123.0/build/three.min.js'></script>
<script src='https://unpkg.com/three@0.123.0/examples/js/loaders/GLTFLoader.js'></script><script  src="./script.js"></script>
</head>
<body>
  <div class="widget-wrap">
  <h1>The final Quizz</h1>
  <div id="quizWrap"></div>
    <div id="code-boxx">
      <a href="" id="ending">Try again</span>
      <a href="{{ route('show_finale', ['id'=>$player->id ]) }}" id="starting">Finish the game !</a>
    </div>
</div>

<div id="canvas"></div>
</body>
</html>
<script>
  
    let scene,
  camera,
  fieldOfView,
  aspectRatio,
  nearPlane,
  farPlane,
  renderer,
  container,
  rocket,
  mixer,
  HEIGHT,
  WIDTH;

const createScene = () => {
  HEIGHT = window.innerHeight;
  WIDTH = window.innerWidth;

  scene = new THREE.Scene();

  scene.fog = new THREE.Fog(0x5d0361, 10, 1500);

  aspectRatio = WIDTH / HEIGHT;
  fieldOfView = 60;
  nearPlane = 1;
  farPlane = 10000;
  camera = new THREE.PerspectiveCamera(
    fieldOfView,
    aspectRatio,
    nearPlane,
    farPlane
  );

  camera.position.x = 0;
  camera.position.z = 500;
  camera.position.y = 50;

  renderer = new THREE.WebGLRenderer({
    alpha: true,
    antialias: true
  });
  renderer.setSize(WIDTH, HEIGHT);

  renderer.shadowMap.enabled = true;

  container = document.getElementById("canvas");
  container.appendChild(renderer.domElement);

  window.addEventListener("resize", handleWindowResize, false);

  let loader = new THREE.GLTFLoader();
    
              loader.load("{{ asset('/storage/3d_objects/boss/scene.gltf') }}", function(gltf){
              const forest = gltf.scene.children[0];
              //Scale the object
              forest.scale.set(80,80,80);
              //Position of the object
              forest.position.set(0,-10,0);
              //Rotation of the object (radian)
              forest.rotation.z=(-4.71);
              scene.add(gltf.scene);
              mixer = new THREE.AnimationMixer(gltf.scene);
               const clips = gltf.animations;
               const clip = THREE.AnimationClip.findByName(clips, 'Walkcycle_Animation');
               const action = mixer.clipAction(clip);
               action.play();
               animate();
               setTimeout(function(){
              animationsecond();
              },5000)
              function animationfirst(){
             
               mixer = new THREE.AnimationMixer(gltf.scene);
               const clips = gltf.animations;
               const clip = THREE.AnimationClip.findByName(clips, 'Walkcycle_Animation');
               const action = mixer.clipAction(clip);
               action.play();
               animate();
                setTimeout(function(){
               animationsecond();
               },5000 )
              }

               function animationsecond(){
              
               mixer = new THREE.AnimationMixer(gltf.scene);
               const clips = gltf.animations;
               const clip = THREE.AnimationClip.findByName(clips, 'Idel_Animation');
               const action = mixer.clipAction(clip);
               action.play();
               animate();
               setTimeout(function(){
               animationfirst();
               },10000 )
              }
              
              
              });
};

const handleWindowResize = () => {
  HEIGHT = window.innerHeight;
  WIDTH = window.innerWidth;
  renderer.setSize(WIDTH, HEIGHT);
  camera.aspect = WIDTH / HEIGHT;
  camera.updateProjectionMatrix();
};

const createLights = () => {
  const ambientLight = new THREE.HemisphereLight(0x404040, 0x404040, 1);

  const directionalLight = new THREE.DirectionalLight(0xdfebff, 1);
  directionalLight.position.set(-300, 0, 600);

  const pointLight = new THREE.PointLight(0xa11148, 2, 1000, 2);
  pointLight.position.set(200, -100, 50);

  scene.add(ambientLight, directionalLight, pointLight);
};

const targetRocketPosition = 40;
const animationDuration = 2000;

const main = () => {
  createScene();
  createLights();
  renderer.render(scene, camera);
 
};

main();
 const clock = new THREE.Clock();
function animate() {

                if(mixer)
                mixer.update(clock.getDelta());
                renderer.render(scene,camera);
                requestAnimationFrame(animate);
             
                }
                animate();
//%ake the smoke of the rocket appear 20 secondes after the user start the page
setTimeout(() => {

 let cam, scene, renderer,
    clock, smokeMaterial,
    h, w,
    smokeParticles = [];

const animate = () => {
        let delta = clock.getDelta();

        requestAnimationFrame(animate);

        [].forEach.call(smokeParticles, sp => {
            sp.rotation.z += delta * 0.2;
        });

        renderer.render(scene, cam);
    },
    resize = () => {
        renderer.setSize(window.innerWidth, window.innerHeight);

    },
    lights = () => {
        const light = new THREE.DirectionalLight(0xcef5c8, 0.5);
        clock = new THREE.Clock();
        renderer = new THREE.WebGLRenderer({
        alpha: true,
        antialias: true
        });
        renderer.setClearColor( 0xffffff, 0);
        renderer.setSize(w, h);

        scene = new THREE.Scene();
        scene.background = new THREE.Color( 0xffffff, 0 );
        light.position.set(-1, 0, 1);
        scene.add(light);
    },
    camera = () => {
        cam = new THREE.PerspectiveCamera(
            75,
            w / h,
            1,
            10000
        );

        cam.position.z = 1000;

        scene.add(cam);
    },
    action = () => {
        const loader = new THREE.TextureLoader();

        loader.crossOrigin = '';

        loader.load(
            'https://s3-us-west-2.amazonaws.com/s.cdpn.io/82015/blue-smoke.png',
            function onLoad(texture) {
                const smokeGeo = new THREE.PlaneBufferGeometry(200, 100);

                smokeMaterial = new THREE.MeshLambertMaterial({
                    map: texture,
                    transparent: true
                });

                for (let p = 0, l = 350; p < l; p++) {
                    let particle = new THREE.Mesh(smokeGeo, smokeMaterial);

                    particle.position.set(
                        Math.random() * 400 - 250,
                        Math.random() * 400 - 250,
                        Math.random() * 900 - 100
                    );

                    particle.rotation.z = Math.random() * 200;
                    scene.add(particle);
                    smokeParticles.push(particle);
                }
                  const spaceTexture = new THREE.TextureLoader().load("{{ asset('/storage/images/apocalypse.jpg') }}");
            scene.background = spaceTexture;

                animate();
            }
        );

    },
    init = () => {
        h = window.innerHeight;
        w = window.innerWidth;
        
        lights(); //💡
        camera(); // 🎥
        action(); // 🎬

        document.body.appendChild(renderer.domElement);
        
    };

init();
}, 3000)

    </script>
    <script>
      var quiz = {
  // (A) PROPERTIES
  // (A1) QUESTIONS & ANSWERS
  // Q = QUESTION, O = OPTIONS, A = CORRECT ANSWER
  data: [
  {
    q : "How much is 18+10",
    o : [
      "8",
      "28", //true//
      "21",
      "7"
    ],
    a : 1 // arrays start with 0, so answer is 70 meters
  },
  {
    q : "Which animal does not come from the farm ?",
    o : [
      "Cow",
      "Pig",
      "Chicken",
      "Cheetah" //true//
    ],
    a : 3
  },
  {
    q : "What is the color of a crocodile ?",
    o : [
      "Red",
      "Yellow",
      "Green", //true//
      "Purple"
    ],
    a : 2
  },
  {
    q : "What's the name of your companion ?",
    o : [
      "Mirallia", //True//
      "Mallia",
      "Maria",
      "Maloya"
    ],
    a : 0
  },
  {
    q : "How many hours are there in a day ?",
    o : [
      "10 hours",
      "22 hours",
      "12 hours",
      "24 hours" //true//
    ],
    a : 3
  }
  ],

  // (A2) HTML ELEMENTS
  hWrap: null, // HTML quiz container
  hQn: null, // HTML question wrapper
  hAns: null, // HTML answers wrapper

  // (A3) GAME FLAGS
  now: 0, // current question
  score: 0, // current score

  // (B) INIT QUIZ HTML
  init: () => {
    // (B1) WRAPPER
    quiz.hWrap = document.getElementById("quizWrap");

    // (B2) QUESTIONS SECTION
    quiz.hQn = document.createElement("div");
    quiz.hQn.id = "quizQn";
    quiz.hWrap.appendChild(quiz.hQn);

    // (B3) ANSWERS SECTION
    quiz.hAns = document.createElement("div");
    quiz.hAns.id = "quizAns";
    quiz.hWrap.appendChild(quiz.hAns);

    // (B4) GO!
    quiz.draw();
  },

  // (C) DRAW QUESTION
  draw: () => {
    // (C1) QUESTION
    quiz.hQn.innerHTML = quiz.data[quiz.now].q;

    // (C2) OPTIONS
    quiz.hAns.innerHTML = "";
    for (let i in quiz.data[quiz.now].o) {
      let radio = document.createElement("input");
      radio.type = "radio";
      radio.name = "quiz";
      radio.id = "quizo" + i;
      quiz.hAns.appendChild(radio);
      let label = document.createElement("label");
      label.innerHTML = quiz.data[quiz.now].o[i];
      label.setAttribute("for", "quizo" + i);
      label.dataset.idx = i;
      label.addEventListener("click", () => { quiz.select(label); });
      quiz.hAns.appendChild(label);
    }
  },

  // (D) OPTION SELECTED
  select: (option) => {
    // (D1) DETACH ALL ONCLICK
    let all = quiz.hAns.getElementsByTagName("label");
    for (let label of all) {
      label.removeEventListener("click", quiz.select);
    }

    // (D2) CHECK IF CORRECT
    let correct = option.dataset.idx == quiz.data[quiz.now].a;
    if (correct) {
      quiz.score++;
      option.classList.add("correct");
    } else {
      option.classList.add("wrong");
    }

    // (D3) NEXT QUESTION OR END GAME
    quiz.now++;
    setTimeout(() => {
      if (quiz.now < quiz.data.length) { quiz.draw(); }
      else {
        quiz.hQn.innerHTML = `You have answered ${quiz.score} of ${quiz.data.length} correctly.`;
        quiz.hAns.innerHTML = "";
        if (quiz.score == 5) {
          var ending = document.getElementById("starting");
          ending.style.display = "block";
        }
        else if (quiz.score < 5) {
           var restart = document.getElementById("ending");
           restart.style.display = "block";
           setTimeout(function() {
              restart.style.display = "none";
              quiz.init();
           },2000)
        
        }

      }
      
    }, 1000);
  },

  // (E) RESTART QUIZ
  reset : () => {
    quiz.now = 0;
    quiz.score = 0;
    quiz.draw();
  }
};
window.addEventListener("load", quiz.init);

      </script>
  </x-app-layout>
