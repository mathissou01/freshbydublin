<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.min.js" defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

@include('partials.loader')
        <div>
            {{ $slot }}
        </div>
         <div>   
                    @if (count($errors) > 0)
                    <div>
                      <ul>
                          @foreach ($errors->all() as $error)
                         <li> {{ $error}} </li>
                          @endforeach
                      </ul>
                    </div> 
                    @endif
                </div> 

