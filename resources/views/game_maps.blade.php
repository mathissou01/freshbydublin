<x-app-layout>
<link rel="stylesheet" href="{{ ('../css/maps.css') }}"> 
<link rel="stylesheet" href="{{ ('../css/footer.css') }}">
<link rel="stylesheet" href="{{ ('../css/loading.css') }}">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Maps - Freshby</title>
</head>
<body>
  {{-- Background audio --}}
  <audio autoplay="true" src="{{ asset('/storage/audio/boss_intro.mp3') }}">
	  Update your browser. Your browser does not support HTML audio
	</audio> 
  {{-- Menu of the game --}}
	<input class="menu-icon" type="checkbox" id="menu-icon" name="menu-icon"/>
  	<label for="menu-icon"></label>
  	<nav class="nav"> 		
  		<ul class="pt-5">
  			<li><a href="#">The hidden animals</a></li>
  			<li><a href="#">The book of calculations</a></li>
  			<li><a href="#">The time tavern</a></li>
  		</ul>
  	</nav>
  {{-- Content in front of the scene/ Title of the game --}}
    <div class="container" id="title_game" style="opacity: 0; transition: all .5s ease-in-out">
      {{-- Display the name of the game or a smyley "done" if the game is completed --}}
      @if($player->game2 == "0")
      <div class="island1"><a href="{{ route('show_game2', ['id'=>$player->id]) }}" class="game2">The hidden animals</a></div>
      @else($player->game2 == "1")
      <div class="island1v1"><span class="spangame">✅</span></div>
      @endif
      @if($player->game3 == "0")
      <div class="island2"><a href="{{ route('show_game3', ['id'=>$player->id]) }}" class="game3">The book of calculations</a></div>
       @else($player->game3 == "1")
       <div class="island2v2"><span class="spangame">✅</span></div>
      @endif
        @if($player->game1 == "0")
      <div class="island3"><a href="{{ route('show_game1', ['id'=>$player->id]) }}" class="game1">The time tavern</a></div>
        @else($player->game1 == "1")
         <div class="island3v3"><span class="spangame">✅</span></div>
          @endif
    </div> 
   
    @if ($player->game1 == "1" && $player->game2 == "1" && $player->game3 == "1")
<div class="boss_finale" style="display:block" id="boss_finale">
  <a class="bosshref" href="{{ route('show_game_boss', ['id'=>$player->id]) }}">BOSS</a><br>
  <span class="bosssentance">You have found all the artefacts, now it is time to defeat the Billytrash</span><br>
  <a href="{{ route('show_game_boss', ['id'=>$player->id]) }}" class="bossclick">Click here</a>
</div>
    @else 
    <div class="boss_finale" style="display:none">
</div>
@endif
    
  {{-- Canvas who display my 3d scene --}}
  <canvas class="forest"></canvas>

</body>
</html>
</x-app-layout>

 <script type="module">
   
      import * as THREE from '../three.js-master/src/Three.js';
      import { OrbitControls } from '../three.js-master/examples/jsm/controls/OrbitControls.js';
      import { GLTFLoader } from '../three.js-master/examples/jsm/loaders/GLTFLoader.js';
      import { EffectComposer } from '../three.js-master/examples/jsm/postprocessing/EffectComposer.js';
      import { RenderPass } from '../three.js-master/examples/jsm/postprocessing/RenderPass.js';
      import { GlitchPass } from '../three.js-master/examples/jsm/postprocessing/GlitchPass.js';
      import { ShaderPass } from '../three.js-master/examples/jsm/postprocessing/ShaderPass.js';
      import { LuminosityShader } from '../three.js-master/examples/jsm/shaders/LuminosityShader.js';
     

      let scene, camera, renderer, mixer, controls, composer;

               
         function init() { 

            scene = new THREE.Scene();
            //Set the position of the camera
            camera = new THREE.PerspectiveCamera(40,window.innerWidth/window.innerHeight,1,5000);

            camera.rotation.set(0, 0, 0);
            camera.position.x = -200;
            camera.position.y = 20;
            camera.position.z = -50;
            camera.lookAt(0, 0, 0);
            renderer = new THREE.WebGLRenderer({
            canvas: document.querySelector('.forest'),
            });
            // Put the 3d scene into a canvas named "forest"
            renderer.setSize( window.innerWidth, window.innerHeight );
            document.body.appendChild( renderer.domElement );

            // Post processing effect
            composer = new EffectComposer(renderer);
            composer.addPass(new RenderPass(scene, camera));
            animate();
           
            controls = new OrbitControls(camera, renderer.domElement);
            controls.addEventListener('change', renderer);
            controls.update();
            
            //Creating virtual light
            const hlight = new THREE.AmbientLight (0x404040,1);
            scene.add(hlight);

            const directionalLight = new THREE.DirectionalLight(0xffffff,1);
            directionalLight.position.set(0,1,0);
            directionalLight.castShadow = true;
            scene.add(directionalLight);
            const light = new THREE.PointLight(0xc4c4c4,1);
            light.position.set(140,200,-1500);
     
            scene.add(light);
            const light2 = new THREE.PointLight(0xc4c4c4,1);
            light2.position.set(0,200,10);
         
            scene.add(light2);
            const light3 = new THREE.PointLight(0xc4c4c4,1);
            light3.position.set(-300,200,1450);
          
            scene.add(light3);
          

            // Background of the scene
            const spaceTexture = new THREE.TextureLoader().load("{{ asset('/storage/images/cloud.jpg') }}");
            scene.background = spaceTexture;

           
                 
             
              // Loader to load my scene
              let loader = new GLTFLoader();
              //Choose the files
              loader.load("{{ asset('/storage/3d_objects/forest/untitled.gltf') }}", function(gltf){
              const forest = gltf.scene.children[0];
              //Scale the object
              forest.scale.set(0.5,0.5,0.5);
              //Position of the object
              forest.position.set(140,-120,-1500);
              //Rotation of the object (radian)
              forest.rotation.z=(0.3);
            
              scene.add(gltf.scene);
            
              });
           
              //Choose the files
              loader.load("{{ asset('/storage/3d_objects/viking/untitled.gltf') }}", function(gltf){
              const viking = gltf.scene.children[0];
              //Scale the object
              viking.scale.set(15,15,15);
              //Position of the object
              viking.position.set(-300,-100,1450);
              //Rotation of the object (radian)
              viking.rotation.z=(3.2);
              scene.add(gltf.scene);
              
              });
              
              loader.load("{{ asset('/storage/3d_objects/castle/scene.gltf') }}", function(gltf){
              const castle = gltf.scene.children[0];
              //Scale the object
              castle.scale.set(15,15,15);
              //Position of the object
              castle.position.set(0,20,10);
              //Rotation of the object (randian)
              castle.rotation.z=(4.6);
              scene.add(gltf.scene);
             
               mixer = new THREE.AnimationMixer(gltf.scene);
               const clips = gltf.animations;
               const clip = THREE.AnimationClip.findByName(clips, 'The Life');
               const action = mixer.clipAction(clip);
               action.play();
              animate();
              });
          
            }
            
                //Loop the animation 
                const clock = new THREE.Clock();
                function animate() {

                if(mixer)
                mixer.update(clock.getDelta());
                renderer.render(scene,camera);
                requestAnimationFrame(animate);
                composer.render(scene,camera);

                }
  
              function easeInOutSine(x) {
                return -(Math.cos(Math.PI * x) - 1) / 2;
              }

              function animateCamera(part) {
                var etape = easeInOutSine(part)

                camera.position.x = -3000 * etape;
                camera.position.z = -380 * etape;
                camera.position.y = 994 * etape;
                camera.rotation.set(-1.91 * etape, -1.22 * etape, -1.93 * etape);
                
                part += 0.01;
                if (part < 1) {
                  setTimeout(animateCamera, 10, part);
                }
                else{
                  document.querySelector("#title_game").style.opacity = "1";
                  document.querySelector("#boss_finale").style.opacity = "1";
                }
                
              }

          init();
          animate();
          animateCamera(0);    
    </script>
  
 