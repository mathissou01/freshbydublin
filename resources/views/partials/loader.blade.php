<link rel="stylesheet" href="{{ ('css/loading.css') }}">
<!DOCTYPE html>
<html lang="en">
<body>
    <div class="loader-wrapper">
        <span class="loader"><span class="loader-inner"></span></span>
    </div>
</body>
</html>
{{--Loading screen--}}
    <script>
        $(window).on("load",function(){
          $(".loader-wrapper").fadeOut("slow");
        });
    </script>