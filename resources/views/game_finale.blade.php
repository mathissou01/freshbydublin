<x-app-layout>
    <link rel="stylesheet" href="{{ ('../css/finale.css') }}">
    <script src="{{ ('../js/game_finale/main.js') }}" defer></script>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Freshby - End</title>
</head>
<body>
    <div class="container">
        <span class="text">
            Good job 
            
            ! You have defeated Billytrash and saved the world !<br>
            Now it is time to say goodbye and end this adventure <br>
        </span>
    </div>
    <canvas class="boss"></canvas>
    <script type="module">
   
      import * as THREE from '../three.js-master/src/Three.js';
      import { OrbitControls } from '../three.js-master/examples/jsm/controls/OrbitControls.js';
      import { GLTFLoader } from '../three.js-master/examples/jsm/loaders/GLTFLoader.js';
      import { EffectComposer } from '../three.js-master/examples/jsm/postprocessing/EffectComposer.js';
      import { RenderPass } from '../three.js-master/examples/jsm/postprocessing/RenderPass.js';
      import { GlitchPass } from '../three.js-master/examples/jsm/postprocessing/GlitchPass.js';
      import { ShaderPass } from '../three.js-master/examples/jsm/postprocessing/ShaderPass.js';
      import { LuminosityShader } from '../three.js-master/examples/jsm/shaders/LuminosityShader.js';
     

      let scene, camera, renderer, mixer, controls, composer;

               
         function init() { 

            scene = new THREE.Scene();
            //Set the position of the camera
            camera = new THREE.PerspectiveCamera(40,window.innerWidth/window.innerHeight,1,5000);

            camera.rotation.set(-0.25, -0.10, 0);
            camera.position.x = -33;
            camera.position.y = 80;
            camera.position.z = 300;
            camera.lookAt(0, 0, 0);

            document.body.addEventListener("click",()=>{
                console.log(camera.position, camera.rotation)
            });

            renderer = new THREE.WebGLRenderer({
            canvas: document.querySelector('.boss'),
            });
            // Put the 3d scene into a canvas named "forest"
            renderer.setSize( window.innerWidth, window.innerHeight );
            document.body.appendChild( renderer.domElement );

            // Post processing effect
            composer = new EffectComposer(renderer);
            composer.addPass(new RenderPass(scene, camera));
            animate();
           
            controls = new OrbitControls(camera, renderer.domElement);
            controls.addEventListener('change', renderer);
            controls.update();
            
            //Creating virtual light
            const hlight = new THREE.AmbientLight (0xfcff,0.9);
            scene.add(hlight);

            const directionalLight = new THREE.DirectionalLight(0xffffff,1);
            directionalLight.position.set(0,220,0);
            directionalLight.castShadow = true;
            scene.add(directionalLight);
            const light = new THREE.PointLight(0xfcff,2, 0);
            light.position.set(-10,220,-10);
     
            scene.add(light);
            const light2 = new THREE.PointLight(0xfcff,2, 0);
            light2.position.set(30,220,30);
         
            scene.add(light2);
            const light3 = new THREE.PointLight(0xfcff,2, 0);
            light3.position.set(0,220,0);
            scene.add(light3);

            const light4 = new THREE.PointLight(0xfcff,2, 0);
            light4.position.set(-30,220,-30);
             scene.add(light4);
                       
              // Loader to load my scene
              let loader = new GLTFLoader();
         
              //Choose the files
              loader.load("{{ asset('/storage/3d_objects/finale/scene.gltf') }}", function(gltf){
              const boss = gltf.scene.children[0];
              //Scale the object
              boss.scale.set(3,3,3);
              //Position of the object
              boss.position.set(0,-30,0);
              //Rotation of the object (radian)
              boss.rotation.z=(0);
              scene.add(gltf.scene);
              //Load the animations of my object
              mixer = new THREE.AnimationMixer(gltf.scene);
              const clips = gltf.animations;
              const clip = THREE.AnimationClip.findByName(clips, 'Experiment');
              const action = mixer.clipAction(clip);    
               //Play the action
              action.play();

              animate();
              });
          
            }
            
                //Loop the animation 
                const clock = new THREE.Clock();
                function animate() {

                if(mixer)
                mixer.update(clock.getDelta());
                renderer.render(scene,camera);
                requestAnimationFrame(animate);
                composer.render(scene,camera);

                }
  
           

          init();
          animate();
                redirect();
          function redirect() {
              setTimeout(function(){
                window.location.href = "{{ route('show_end', ['id'=>$player->id]) }}";
              },19000)
          }
    </script> 
</body>
</html>
</x-app-layout>