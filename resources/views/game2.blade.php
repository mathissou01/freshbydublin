<x-app-layout>
  <link rel="stylesheet" href="{{ ('../css/game2.css') }}">
     <script src="{{ ('../js/game2/animals.js') }}" defer></script>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Freshby - Find the animals</title>
</head>
<body>
   
<div id="myModal" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <p>Click on the cards to flip them <br> and match the animals !</p>
    <img src="{{ asset('/storage/images/animals/tuto_animals.svg') }}" class='tutorial'>
  </div>
</div>
<div class='board'></div>
<div class='clone' id='board'>
  <div class='face'></div>
  <div class='face'></div>
  <div class='face'></div>
  <div class='face'></div>
  <div class='face'></div>
  <div class='face'></div>
</div>
<div class='overlay hidden'>
  <div class='gameover'>
    {{-- Text variable of the button --}}
    <p id="test1"></p> 
    <button class='reset'>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="747.8 72.3 1008 880" enable-background="new 747.8 72.3 1008 880"><path fill="#fff" d="M1742.2 510l-427.4 427.6-97.4-98.8 261.2-259.8h-718v-138.2h718l-259.8-258.4 97.2-98.6 426.2 426.2z"/></svg>
</button>
    <form action="{{ route('completegame2') }}" method="POST">
        @csrf
      <input type="hidden" name="id" value="{{ $id }}">
        <button class="submit" type="submit" id="submit">
        Enregistrer
        </button>
      </form>
  </div>
  
</div>

</body>
</html>
</x-app-layout>