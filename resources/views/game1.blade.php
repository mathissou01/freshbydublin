<x-app-layout>
    <link rel="stylesheet" href="{{ ('../css/game1.css') }}">
    <script src="{{ ('../js/game1/main.js') }}" defer></script>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Freshby - The clock</title>
    </head>
<body>
         
        <form action="{{ route('completegame1') }}" method="POST">
    @csrf
   <input type="hidden" name="game1" id="game1" value="1">
   <input type="hidden" name="id" value="{{ $id }}">
    <button type="submit" class="submit" style="display: none">
        Return to the maps
    </button>
  </form>
  <section class="quizz-wrapper">
        <img id="heure1" src="{{ asset('/storage/images/clock/heure1.png') }}">
		<div class="quizz">
			<div class="card question-wrapper active" data-question="1">
                
				<div class="header">
                    <h1>Guess !</h1>
				</div>
				<div class="question">
					What time is it ?
				</div>
				<div class="answers">
					<a href="#" data-answer="a">12H00</a>
					<a href="#" data-answer="b">10H20</a>
					<a href="#" data-answer="c">15H16</a>
				</div>
			</div>
			<div class="card answer">
				<div class="header"></div>
				<div class="answer"></div>
				<div class="reset"></div>
			</div>
		</div>
	</section>
      
<script>
      let btnAnswers = document.querySelector('.question-wrapper .answers');
let cardAnswer = document.querySelector('.card.answer');
let divReset = document.querySelector('.card.answer .reset');
let submit = document.querySelector('.submit');

btnAnswers.addEventListener('click', function(e){
	e.preventDefault();
	var userAnswer = e.target.getAttribute("data-answer").toLowerCase();
	document.querySelector('.question-wrapper').classList.remove('active');
	cardAnswer.classList.add('active');
	
	switch(userAnswer) {
  case "a":
    updateAnswer(false, "Incorrect, try again.");
    break;
  case "b":
    updateAnswer(true, "Correct ! It is indeed 10H20.");     
    break;
  case "c":
    updateAnswer(false, "Incorrect, try again.");
    break;
  default:
    updateAnswer(false, "Incorrect, try again.");
	} 
});
divReset.addEventListener('click', function(e){
	e.preventDefault();
	cardAnswer.querySelector(".header").innerHTML = "";
	cardAnswer.querySelector(".answer").innerHTML = "";
	cardAnswer.classList.remove('active');
	cardAnswer.classList.remove('false');
	cardAnswer.classList.remove('true');
  divReset.innerHTML = "";
	document.querySelector('.question-wrapper.card').classList.add('active');
})

function updateAnswer(status, bodyMsg){
  if(status === false){
    cardAnswer.classList.add('false');
    cardAnswer.querySelector('.header').append("Incorrect, try again.");
    var response = document.createElement("p");
    var responseText = document.createTextNode(bodyMsg);
    var btnReset = document.createElement("a");
    btnReset.setAttribute('href', '#');
    var btnResetText = document.createTextNode("Try again ?");
    btnReset.appendChild(btnResetText);
    divReset.appendChild(btnReset);
    response.appendChild(responseText);
    cardAnswer.querySelector('.answer').appendChild(response);
  }else{
    cardAnswer.classList.add('true');
    cardAnswer.querySelector('.header').append("Correct ! It is indeed 10H20.");
    submit.style.display = "block";
    var response = document.createElement("p");
    var responseText = document.createTextNode(bodyMsg);
    var congrats = document.createElement("h1");
    var congratsText = document.createTextNode("Congratulations!");
    congrats.appendChild(congratsText);
    response.appendChild(responseText);
    cardAnswer.querySelector('.answer').appendChild(response);
    divReset.innerHTML = "";
    divReset.appendChild(congrats);
  }
}
        </script>
    </body>

    </html>
</x-app-layout>