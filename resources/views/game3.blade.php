<x-app-layout>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ ('../css/game3.css') }}">
    <script src="{{ ('../js/game3/main.js') }}" defer></script>
    <title>Freshby - The calculus castle</title>
</head>
<body>
   <form action="{{ route('completegame3') }}" method="POST">
    @csrf
   <input type="hidden" name="game3" id="game3" value="1">
   <input type="hidden" name="id" value="{{ $id }}">
  <button class="button3" id="button3" type="submit">Return to the map</button> 
  </form>
    </br><center><div class="container" id="container">
<img src="{{ asset('/storage/images/castle.png')}}" id="background">
<div class = "cloud1" id = "cloud1"><div class = "cloud_1" id = "cloud_1">
  <div id = cloud1Shadow>
    <div id = "circle1Shadow"></div><div id = "circle2Shadow"></div>
    <div id = "circle3Shadow"></div><div id = "circle4Shadow"></div>
    <div id = "rectShadow"></div>
  </div>
    <div id = "circle1"></div><div id = "circle2"></div>
    <div id = "circle3"></div><div id = "circle4"></div>
    <div id = "rect"></div>
    <div id = "text1">5+7+9</div></div></div>

<div class = "cloud2" id = "cloud2"><div class = "cloud_2" id = "cloud_2">
  <div id = cloud2Shadow>
    <div id = "circle1Shadow_2"></div><div id = "circle2Shadow_2"></div>
    <div id = "circle3Shadow_2"></div><div id = "circle4Shadow_2"></div>
    <div id = "rectShadow_2"></div>
  </div>
    <div id = "circle1_2"></div><div id = "circle2_2"></div>
    <div id = "circle3_2"></div><div id = "circle4_2"></div>
    <div id = "rect_2"></div>
    <div id = "text2">2+2+7+9</div></div></div>
  
 <input id="input1" type="text" size="15" oninput="validateInput(this)" maxlength="10" onkeydown = "if (event.keyCode == 13)
                        document.getElementById('button1').click()"/>
   
<span class="button1" id="button1" style="box-shadow: 0px 1px 0px 0px rgb(187, 218, 247) inset; background: rgb(103, 208, 246) linear-gradient(to bottom, rgb(103, 208, 246) 5%, rgb(61, 153, 184) 100%) repeat scroll 0% 0%; border-radius: 9px; display: inline-block; cursor: pointer; color: rgb(255, 255, 255); font-family: Verdana; font-size: 14pt; padding: 6px 24px; text-decoration: none; text-shadow: 0px 1px 0px rgb(60, 143, 173);" onclick="submit()">Submit</span>  
  
<div id = "score">Score: 0</div>  
  

  
<span class="button2" id="button2" onclick="start(), focusMethod()">Start game</span> 


<div id = "gameOver">Game over! Your score is 0 :)</div>  
   
</div>
  
</center>

</body>
</html>
</x-app-layout>
<script>
  // When the user click on the button it make the focus into the input
  focusMethod = function getFocus() {
  document.getElementById("input1").focus();
  $("#formToDisable :input").prop("disabled", true);
  FocusForced = true;
  // Even listener that if the user is leaving the input then it will put back the focus into the input
document.getElementById("input1").addEventListener("focusout", function() {
          if (FocusForced == true){
            setTimeout(() => {
              document.getElementById("input1").focus();
            }, 100);
          }
        });
        }
</script>