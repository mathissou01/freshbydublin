
<x-app-layout>
<link rel="stylesheet" href="{{ ('css/main.css') }}">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Freshby - Kids Game</title>
</head>
    <body>
        <div class="play">
            <div class="logo">
                <img src="{{ asset('/storage/images/logo/staff_logo.svg') }}" class="logobutton" id="staff">
                <img src="{{ asset('/storage/images/logo/sword_logo.svg') }}" class="logobutton" id="sword">
            </div>
            <button id="myBtn" class="button">
               <div></div>
            </button>
             <audio src="{{ asset('/storage/audio/bouton.mp3') }}" id="myAudio"></audio>
        </div> 
    </body>
</html>
</x-app-layout>
{{-- Script that play the sound and redirect to the next page--}}
<script>  
    document.getElementById("myBtn").addEventListener("click",function(){
    document.getElementById("myAudio").play();
    setTimeout(() => {
        document.location.href = "{{ route('new_player') }}";
    }, 1000 );
    }
    );
</script>
@include('partials.footer')