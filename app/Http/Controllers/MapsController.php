<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Maps;
use App\Models\Player;

class MapsController extends Controller
{
    public function showmaps($id){
        return view('game_maps', ["player" => Player::find($id)]);
    }
    public function introduction($id){
    return view('game_intro', ["player" => Player::find($id)]);
    }
    
     public function game1(Request $request, $id){
        $player = Player::where('id',$id)->first();
        if ($player) {
            return view('game1',['game1'=>$player->game1, 
            'id'=>$player->id]);
        }
        else {
          connectify('error','Erreur',"Erreur l'étudiant' que vous souhaitez modifier n'éxiste pas");
          return redirect('/introduction');
        }
    }
     public function game2(Request $request, $id){
        $player = Player::where('id',$id)->first();
        if ($player) {
            return view('game2',['game2'=>$player->game2, 
            'id'=>$player->id]);
        }
        else {
          connectify('error','Erreur',"Erreur l'étudiant' que vous souhaitez modifier n'éxiste pas");
          return redirect('/introduction');
        }
    }
     public function game3(Request $request, $id){
        $player = Player::where('id',$id)->first();
        if ($player) {
            return view('game3',['game3'=>$player->game3, 
            'id'=>$player->id]);
        }
        else {
          connectify('error','Erreur',"Erreur l'étudiant' que vous souhaitez modifier n'éxiste pas");
          return redirect('/introduction');
        }
    }



    public function landing_page(){
        return view('landing_page');
    }
      public function gameboss($id){
        return view('game_boss', ["player" => Player::find($id)]);
    }
     public function finale($id){
        return view('game_finale', ["player" => Player::find($id)]);
    }
     public function end($id){
         return view('ending_text', ["player" => Player::find($id)]);
     }
}
