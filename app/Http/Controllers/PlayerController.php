<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class PlayerController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
     public function newplayer(){
            return view('pregame_form');
     }

     public function storeplayer(Request $request){
            $messages = [
          'required' => 'You forgot to put your ":attribute"',
          'string' => ':attribute is not correct',
          'integer' => ':attribute is not correct',
        ];

        $attributes = [
          'first_name' => 'Fistname',
          'place' => 'Place',
          'gender' => 'Gender',
        ];

        $validateData = Validator::make($request->all(), [
          'first_name' => 'required | string',
          'place' => 'required | string',
          'gender' => 'required | string',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect("/new_player");
        }
        else {
          $player = new Player;
          $player->first_name = $request->first_name;
          $player->place = $request->place;
          $player->gender = $request->gender;
          $player->save();
          auth()->id();
          return redirect("/introduction/$player->id");
          // return redirect("/familly/$familly->id/student/new")->with(['familly_id'=>$familly->id]);
        }
    }
public function complete_game2(Request $request){
      $messages = [
          
          'integer' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'game2' => 'Game2',
        ];

        $validateData = Validator::make($request->all(), [
          'game2' => 'required | integer',
          
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect(url()->previous());
        }
        else {
          $player = Player::where('id',$request->id)->first();
          if ($player) {
              $player->game2 = $request->game2;
              $player->save();
               auth()->id();
          }
          else {
              
          }
          return redirect("/maps/$player->id");
          //  return view('appel', ["appels" => Appel::all()]);
        }
    }

    public function complete_game3(Request $request){
      $messages = [
          
          'integer' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'game3' => 'Game3',
        ];

        $validateData = Validator::make($request->all(), [
          'game3' => 'required | integer',
          
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect(url()->previous());
        }
        else {
          $player = Player::where('id',$request->id)->first();
          if ($player) {
              $player->game3 = $request->game3;
              $player->save();
               auth()->id();
          }
          else {
              
          }
          return redirect("/maps/$player->id");
          //  return view('appel', ["appels" => Appel::all()]);
        }
    }

     public function complete_game1(Request $request){
      $messages = [
          
          'integer' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'game1' => 'Game1',
        ];

        $validateData = Validator::make($request->all(), [
          'game1' => 'required | integer',
          
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect(url()->previous());
        }
        else {
          $player = Player::where('id',$request->id)->first();
          if ($player) {
              $player->game1 = $request->game1;
              $player->save();
               auth()->id();
          }
          else {
              
          }
          return redirect("/maps/$player->id");
          //  return view('appel', ["appels" => Appel::all()]);
        }
    }



}
