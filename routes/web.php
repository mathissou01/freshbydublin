<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\MapsController;


/*Landing page */
Route::get('/', function () {
return view('welcome');});
    
/*Redirect to the form */
Route::get('/new_player', [PlayerController::class, 'newplayer'])->name('new_player');

/* Saving the form */
Route::post('/add_player', [PlayerController::class, 'storeplayer'])->name('store_player');

/* Saving the info of the game2 */
Route::post('/complete_game2', [PlayerController::class, 'complete_game2'])->name('completegame2');

/* Saving the info of the game3 */
Route::post('/complete_game3', [PlayerController::class, 'complete_game3'])->name('completegame3');

/* Saving the info of the game1 */
Route::post('/complete_game1', [PlayerController::class, 'complete_game1'])->name('completegame1');

/* Redirect to the introduction */
Route::get('/introduction/{id}', [MapsController::class, 'introduction'])->name('game_intro');

/* Redirect to the maps */
Route::get('/maps/{id}', [MapsController::class, 'showmaps'])->name('show_maps');

/* Redirect to the game1 */
Route::get('/game1/{id}', [MapsController::class, 'game1'])->name('show_game1');

/* Redirect to the game2 */
Route::get('/game2/{id}', [MapsController::class, 'game2'])->name('show_game2');

/* Redirect to the game3 */
Route::get('/game3/{id}', [MapsController::class, 'game3'])->name('show_game3');

/* Redirect to the home */
Route::get('/home', [MapsController::class, 'landing_page'])->name('landing');

/* Redirect to the gamefinale */
Route::get('/game_boss/{id}', [MapsController::class, 'gameboss'])->name('show_game_boss');

Route::get('/finale/{id}', [MapsController::class, 'finale'])->name('show_finale');

Route::get('/ending/{id}', [MapsController::class, 'end'])->name('show_end');




