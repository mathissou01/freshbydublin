var cloud1 = document.getElementById("cloud1");
var cloud2 = document.getElementById("cloud2");
var input1 = document.getElementById("input1");
var gameOver = document.getElementById("gameOver");

var cloud1_1 = cloud1.cloneNode(true);
var cloud1_2 = cloud1.cloneNode(true);
var cloud1_3 = cloud2.cloneNode(true);
var cloud1_4 = cloud2.cloneNode(true);
var cloud1_5 = cloud1.cloneNode(true);
var cloud1_6 = cloud2.cloneNode(true);
var cloud1_7 = cloud2.cloneNode(true);
var cloud1_8 = cloud2.cloneNode(true);
var cloud1_9 = cloud2.cloneNode(true);
var cloud1_10 = cloud2.cloneNode(true);
var cloud1_11 = cloud2.cloneNode(true);
var cloud1_12 = cloud2.cloneNode(true);
var cloud1_13 = cloud2.cloneNode(true);
var cloud1_14 = cloud2.cloneNode(true);

var submitButton = document.getElementById("button1");
var startButton = document.getElementById("button2");
var finishButton = document.getElementById("button3");
var score = document.getElementById("score");
var answer1 = -1,
    answer2 = -1,
    answer3 = -1;

var answers = [answer1, answer2, answer3];
var a, b, c, d;
var sample1, sample2, sample3;
var samples = [sample1, sample2, sample3];
var current_i = 0;

var clY = -700;

var gameScore = 0;
var cl_timeout = 100;

var cl_speed = 0.5; //cloud speed
var cl_maxI = 300; //iterations count
var cl_opacity = 10; //primary opacity
var cl_opStep = 0.00825; //opacity reduce step
var cl_clouds = [
    cloud1_1,
    cloud1_2,
    cloud1_3,
    cloud1_4,
    cloud1_5,
    cloud1_6,
    cloud1_7,
    cloud1_8,
    cloud1_9,
    cloud1_10,
    cloud1_11,
    cloud1_12,
    cloud1_13,
    cloud1_14,
];
var cl_i = 0;
var firstTimeout = true;
var submitVerify = true;

function start() {
    gameScore = 0;
    score.innerHTML = "Score: 0";
    cl_timeout = 100;
    cl_speed = 0.5;
    cl_maxI = 300;
    cl_opacity = 10;
    cl_opStep = 0.00825;
    cl_i = 0;
    firstTimeout = true;
    submitVerify = true;

    startButton.style.visibility = "hidden";
    submitButton.style.visibility = "visible";
    finishButton.style.visibility = "hidden";

    input1.style.visibility = "visible";
    score.style.visibility = "hidden";
    gameOver.style.visibility = "hidden";

    for (var z = 0; z < 14; z++) {
        try {
            cl_clouds[z].style.visibility == "hidden";
            document.getElementById("container").removeChild(cl_clouds[z]);
        } catch (err) {}
    }

    for (var c = 0; c < 14; c++) {
        cl_clouds[c].setAttribute("data-foo", "-");
        cl_clouds[c].setAttribute("data-foo1", c);
        cl_clouds[c].setAttribute("data-ifsubmitted", false);
    }

    runClouds();
}

function runClouds() {
    current_i = 0;
    cloudMath();
    console.log("Dear cheater! The answear is: " + answers[current_i]);

    try {
        if (cl_i >= 0 && cl_i < 7) {
            document
                .getElementById("container")
                .removeChild(cl_clouds[cl_i + 7]);
            cl_clouds[cl_i + 7].style.visibility = "hidden";
            cl_clouds[cl_i + 7].dataset.ifsubmitted = false;
            cl_clouds[cl_i + 7].setAttribute("data-foo", "-");
        }
        if (cl_i >= 7 && cl_i < 14) {
            document
                .getElementById("container")
                .removeChild(cl_clouds[cl_i - 7]);
            cl_clouds[cl_i - 7].style.visibility = "hidden";
            cl_clouds[cl_i - 7].dataset.ifsubmitted = false;
            cl_clouds[cl_i - 7].setAttribute("data-foo", "-");
        }
    } catch (err) {}

    if (gameScore >= 2 && gameScore < 4) {
        current_i = 1;
    }

    if (gameScore >= 4 && gameScore <= 6) {
        current_i = 2;
    }

    if (gameOver.style.visibility == "hidden") {
        cl_clouds[cl_i].style.visibility = "visible";
        cl_clouds[cl_i].querySelectorAll("div:last-child")[2].innerHTML =
            samples[current_i];
        document.getElementById("container").appendChild(cl_clouds[cl_i]);
        cl_clouds[cl_i].setAttribute("data-foo", cl_i);
    }

    cloudMove(
        cl_clouds[cl_i],
        -310,
        clY,
        cl_speed,
        cl_maxI,
        10,
        cl_opacity,
        cl_opStep
    );

    if (cl_i < 13) {
        cl_i += 1;
    } else {
        cl_i = 0;
    }
}

var x;

function submit() {
    if (submitVerify == true) {
        console.log(cl_i);
        if (cl_i == 0) {
            x = 13;
        } else {
            x = cl_i - 1;
        }

        if (answers[current_i] == input1.value) {
            gameScore += 1;
            score.innerHTML = "Score: " + gameScore;

            document.getElementById("container").removeChild(cl_clouds[x]);
            cl_clouds[x].dataset.ifsubmitted = true;
            cl_clouds[x].setAttribute("data-foo", "-");

            if (gameScore == 7) {
                return gameover();
            }

            cl_timeout = 100;
            runClouds();

            submitVerify = false;
            setTimeout(function () {
                submitVerify = true;
            }, 1000);
        }
    }

    input1.value = "";
    return gameScore;
}

//cloudMove parameters: Cloud name, i0, y offset, cloud speed, iterations count, timeout size, opacity0, opacity step
function cloudMove(cloud, i, y, speed, iterations, time, opacity, opacityStep) {
    setTimeout(function () {
        cloud.style.transform = "translate(" + i + "px," + y + "px)";
        cloud.style.opacity = opacity;

        if (i < iterations) {
            i += speed;
            opacity -= opacityStep;
            cloudMove(
                cloud,
                i,
                y,
                speed,
                iterations,
                time,
                opacity,
                opacityStep
            );
        } else {
            //Cloud finished its way, no answer
            if (
                document
                    .getElementById("container")
                    .querySelector(
                        '[data-foo1="' + cloud.dataset.foo1 + '"]'
                    ) != null
            ) {
                cl_timeout = 100;
                runClouds();
            }
        }
    }, time);
}

function validateInput(inp) {
    inp.value = inp.value.replace(/[^\d]*/g, "");
}

function gameover() {
    finishButton.style.visibility = "visible";
    gameOver.style.visibility = "visible";
    submitButton.style.visibility = "hidden";
    input1.style.visibility = "hidden";
    score.style.visibility = "hidden";
    gameOver.innerHTML = "Congratulations, you did it!";

    for (var z = 0; z < 14; z++) {
        try {
            cl_clouds[z].style.visibility == "hidden";
            document.getElementById("container").removeChild(cl_clouds[z]);
        } catch (err) {}
    }
}

function cloudMath() {
    //a+b, a-b, c*d
    a = Math.trunc(Math.random() * (9 - 1) + 1);
    b = Math.trunc(Math.random() * (9 - 1) + 1);
    c = Math.trunc(Math.random() * (5 - 1) + 1);
    d = Math.trunc(Math.random() * (5 - 1) + 1);

    sample1 = a + " + " + b;
    answer1 = a + b; //a * b
    sample2 = a + 10 + " - " + b;
    answer2 = a + 10 - b; //a - b
    sample3 = c + " * " + d;
    answer3 = c * d; //c * d

    answers = [answer1, answer2, answer3];
    samples = [sample1, sample2, sample3];
}
