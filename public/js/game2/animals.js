const board = document.querySelector(".board");
const clone = document.querySelector(".clone");
const overlay = document.querySelector(".overlay");
const reset = document.querySelector(".reset");
const submit = document.querySelector(".submit");
var numWins = 0;
const tileOptionstuto = ["crocodile", "elephant"];
const tileOptionsOne = ["crocodile", "elephant", "giraffe", "lion"];
const tileOptions = [
    "crocodile",
    "elephant",
    "giraffe",
    "lion",
    "monkey",
    "snake",
];

var modal = document.getElementById("myModal");

const state = {
    selections: [],
    boardLocked: false,
    matches: 0,
};

reset.addEventListener("click", () => {
    if (state.boardLocked) return;
    numWins = numWins += 1;
    if (numWins == 1) {
        modal.style.display = "none";
        resetGame(tileOptionsOne);
    } else if (numWins == 2) {
        resetGame(tileOptions);
        document.getElementById("test1").innerHTML =
            "<span>Return to map</span>";
        submit.style.display = "block";
    }
});

function resetGame(tiler) {
    state.boardLocked = true;
    state.selections = [];
    state.matches = 0;

    document.querySelectorAll(".cube").forEach((tile) => {
        tile.removeEventListener("click", () => selectTile(tile));
        tile.remove();
    });

    overlay.classList.add("hidden");
    createBoard(tiler);
}

function createBoard(tiler) {
    const tiles = shuffleArray([...tiler, ...tiler]);
    const length = tiles.length;

    for (let i = 0; i < length; i++) {
        window.setTimeout(() => {
            board.appendChild(buildTile(tiles.pop(), i));
        }, i * 100);
    }

    window.setTimeout(() => {
        document.querySelectorAll(".cube").forEach((tile) => {
            tile.addEventListener("click", () => selectTile(tile, tiler));
        });

        state.boardLocked = false;
    }, tiles.length * 100);
}

function buildTile(option, id) {
    const tile = clone.cloneNode(true);
    tile.classList.remove("clone");
    tile.classList.add("cube");
    tile.setAttribute("data-tile", option);
    tile.setAttribute("data-id", id);
    return tile;
}

function selectTile(selectedTile, matcher) {
    if (state.boardLocked || selectedTile.classList.contains("flipped")) return;

    state.boardLocked = true;

    if (state.selections.length <= 1) {
        selectedTile.classList.add("flipped");
        state.selections.push({
            id: selectedTile.dataset.id,
            tile: selectedTile.dataset.tile,
            el: selectedTile,
        });
    }

    if (state.selections.length === 2) {
        if (state.selections[0].tile === state.selections[1].tile) {
            setTimeout(function () {
                document.body.style.background = "green";
            }, 100);

            setTimeout(() => {
                document.body.style.background =
                    'url("/storage/images/scoob.jpg")';
                document.body.style.backgroundRepeat = "no-repeat !important";
                document.body.style.backgroundSize = "cover !important";
            }, 1000);

            window.setTimeout(() => {
                state.selections[0].el.classList.add("matched");
                state.selections[1].el.classList.add("matched");

                state.boardLocked = false;
                state.matches = state.matches + 1;

                if (state.matches === matcher.length) {
                    if (numWins < 2) {
                        window.setTimeout(() => {
                            overlay.classList.remove("hidden");
                            document.getElementById("test1").innerHTML =
                                "<span>Next Level</span>";
                        }, 600);
                    } else {
                        window.setTimeout(() => {
                            overlay.classList.remove("hidden");
                            reset.style.display = "none";
                        }, 600);
                    }
                }
                state.selections = [];
            }, 600);
        } else {
            setTimeout(function () {
                document.body.style.background = "red";
            }, 100);

            setTimeout(() => {
                document.body.style.background =
                    'url("/storage/images/scoob.jpg")';
            }, 1000);
            setTimeout(() => {
                document.querySelectorAll(".cube").forEach((tile) => {
                    tile.classList.remove("flipped");
                });
                state.boardLocked = false;
            }, 800);
            state.selections = [];
        }
    } else {
        state.boardLocked = false;
    }
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

createBoard(tileOptionstuto);
