<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\player;

class PlayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Player::create([
        "first_name"=>"John",
        "place"=>"Ville",
        "gender"=>"Fille",
        ]);
        
    }
}
